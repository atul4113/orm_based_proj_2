from django.db import models
from django.conf import settings

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    mobile_no = models.CharField(max_length=25)
    add = models.CharField(max_length=250)
    course = models.CharField(max_length=20)
    gender = models.CharField(max_length=20)
    photo = models.ImageField(upload_to='users/%Y/%m/%d/',blank=True)

    def __str__(self):
         return 'Profile for user {}'.format(self.user.username)